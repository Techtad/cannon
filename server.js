var http = require("http")
var fs = require("fs")
var socketio = require("socket.io")

function sendFile(fileName, res) {
    let directory = "static"
    if (fileName == "/") fileName = "/index.html"
    if (fileName.includes("/libs")) directory = __dirname
    fs.readFile(`${directory}${fileName}`, function (error, data) {
        if (error) {
            console.log(error)
            res.writeHead(404, { "Content-Type": "text/html;charset=utf-8" })
            res.end(`Błąd 404: Nie znaleziono pliku ./static${fileName}`)
        } else {
            let ext = fileName.split(".")[1]
            let contentType = "text/html;charset=utf-8"
            switch (ext) {
                case "js":
                    contentType = "application/javascript"
                    break
                case "css":
                    contentType = "text/css"
                    break
                case "jpg":
                    contentType = "image/jpeg"
                    break
                case "mp3":
                    contentType = "audio/mpeg"
                    break
                case "png":
                    contentType = "image/png"
                    break
                case "svg":
                    contentType = "image/svg+xml"
                    break
                case "gif":
                    contentType = "image/gif"
                    break
                default: break
            }
            res.writeHead(200, { "Content-Type": contentType })
            res.end(data)
        }
    })
}

function servResponse(req, res) {
    let allData = ""
    req.on("data", function (data) {
        allData += data
    })
    req.on("end", function (data) {
        console.log("Data(" + req.url + "):" + allData)
        let obj
        if (allData) {
            try {
                obj = JSON.parse(allData)
            } catch (error) {
                console.log(error)
                res.writeHead(400, { "Content-Type": "text/html;charset=utf-8" })
                res.end(error)
            }
        }
        switch (req.url) {
            default:
                console.log("Błędny POST: " + req.url)
                res.writeHead(400, { "Content-Type": "application/json;charset=utf-8" })
                res.end("Błędna akcja POST: " + req.url)
                break
        }
    })
}

var server = http.createServer(function (req, res) {
    console.log(req.method + " : " + req.url)
    switch (req.method) {
        case "GET":
            sendFile(decodeURI(req.url).split('?')[0], res)
            break;
        case "POST":
            servResponse(req, res)
            break;
    }
})

const port = 3000
server.listen(3000, function () {
    console.log("Start serwera na porcie " + port)
})

var socketServer = socketio.listen(server)
console.log("Start Socket.io")

var clientIds = []
function addClientId(id) {
    let i = 0
    while (clientIds[i]) i++
    clientIds[i] = id
}

var cannonInfo = []
var cannonCount = function () {
    let count = 0
    for (let c of cannonInfo) if (c != null && c != undefined) count++
    return count
}

socketServer.on("connection", function (client) {
    console.log("Klient się połączył: " + client.id)
    addClientId(client.id)
    cannonInfo[clientIds.indexOf(client.id)] = { cannonRot: 0, barrelRot: 0, velocity: 50 }
    client.emit("onconnect", { id: client.id, cannonIndex: clientIds.indexOf(client.id), cannonCount: cannonCount(), cannonInfo: cannonInfo })

    client.on("disconnect", function () {
        cannonInfo[clientIds.indexOf(client.id)] = undefined
        this.broadcast.emit("onleft", { cannonIndex: clientIds.indexOf(client.id) })
        clientIds[clientIds.indexOf(this.id)] = null
        console.log("Klient się rozłączył", this.id)
    })

    client.broadcast.emit("onjoined", { cannonIndex: clientIds.indexOf(client.id) })

    client.on("update", function (data) {
        cannonInfo[clientIds.indexOf(this.id)] = { cannonRot: data.cannonRot, barrelRot: data.barrelRot, velocity: data.velocity }
        this.broadcast.emit("update", { cannonIndex: clientIds.indexOf(this.id), cannonRot: data.cannonRot, barrelRot: data.barrelRot, velocity: data.velocity })
    })
    client.on("shoot", function (data) {
        this.broadcast.emit("shoot", { cannonIndex: clientIds.indexOf(this.id) })
    })
})